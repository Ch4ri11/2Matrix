﻿// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <algorithm>
using namespace std;
void print(int** arr, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
}
void deletearr(int** arr, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        delete[] arr[i];
    };
    delete arr;
}
int main()
{
    int rows, cols;
    cin >> rows >> cols;
    int** arr = new int* [rows];
    for (int i = 0; i < rows; i++) {
        arr[i] = new int[cols];
    }
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            arr[i][j] = rand() % 20;
        }
    }

    print(arr, rows, cols);
    cout << '\n';
    //std::sort(arr[0], arr[rows], comp);
    std::sort(&arr[0], &arr[rows], [](int* row1, int*row2) {
        int sum1 = 0, sum2 = 0;
        for (int i = 0; i < sizeof(*row1); i++) {
            if (i % 2 == 0) {
                sum1 += row1[i];
                sum2 += row2[i];
            }
        }
        return sum1 > sum2;
        });
    print(arr, rows, cols);
    deletearr(arr, rows, cols);
    print(arr, rows, cols);
}



    

